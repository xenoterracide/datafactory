package com.xenoterracide.datafactory;

import com.xenoterracide.util.reflect.ClassUtils;
import com.xenoterracide.util.reflect.MethodUtils;
import org.junit.Test;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assume.assumeThat;

public class RandomStringFactoryTest {
    private static final int ITERATIONS = 100;
    private static final DataFactory RANDOM_DF = DataFactoryImpl.defaultInstance();


    @Test
    public void testGetMaxLength() throws Exception {
        for ( int i = 1; i < ITERATIONS; i++ ) {
            testGetMaxLength( i );
        }
    }

    private void testGetMaxLength( final int length ) {
        for ( int i = 0; i < ITERATIONS; i++ ) {

        }
    }

    @Test
    public void testAll() throws Exception {
        Class<RandomStringFactory> retClass = RandomStringFactory.class;
        List<Method> collect
                = ClassUtils.findAllMethods( RANDOM_DF.getClass() )
                .filter( method -> method.getReturnType().equals( retClass ) )
                .collect( Collectors.toList() );


        for ( Method method : collect ) {
            for ( int i = 1; i < ITERATIONS; i++ ) {
                assumeThat( i, not( 0 ) );
                testFactory( MethodUtils.invoke( retClass, RANDOM_DF, method ), method.getName(), i );
            }
        }
    }

    private void testFactory( final RandomStringFactory factory, final String name, final int length ) {
        for ( int i = 0; i < ITERATIONS; i++ ) {
            String word = factory.getLength( length );

            String fmt = "%s length %d: '%s'";
            assertThat( String.format( fmt, name, length, word ), word, not( isEmptyOrNullString() ) );
            assumeThat( String.format( fmt, name, length, word ), word.length(), equalTo( length ) );

            fmt = "%s maxLen %d: '%s'";
            String maxLengthWord = factory.getMaxLength( length );
            assumeThat( String.format( fmt, name, length, maxLengthWord ), maxLengthWord,
                    not( isEmptyOrNullString() ) );
            assertThat( String.format( fmt, name, length, maxLengthWord ),
                    maxLengthWord.length(), allOf( greaterThanOrEqualTo( 1 ), lessThanOrEqualTo( length ) ) );

            fmt = "%s range %d: '%s'";
            assumeThat( String.format( fmt, name, length, maxLengthWord ), factory.getRange( 1, length ),
                    not( isEmptyOrNullString() ) );
        }
    }
}
