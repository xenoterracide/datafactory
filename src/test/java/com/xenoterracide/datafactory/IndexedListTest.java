package com.xenoterracide.datafactory;

import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class IndexedListTest {
    List<String> list;

    @Before
    public void setup() {
        list = IndexedList.createFrom( Arrays.asList( "cc", "b", "ddd", "a") );
    }

    @Test
    public void testCreateFrom() throws Exception {

    }

    @Test
    public void testLastIndexOf() throws Exception {
        assertThat( list.lastIndexOf( 2 ), is( 3 ) );
        assertThat( list.lastIndexOf( 4 ), is( -1 ) );
    }

    @Test
    public void testIndexOf() throws Exception {
        assertThat( list.indexOf( 1 ), is( 0 ) );
        assertThat( list.indexOf( 4 ), is( -1 ) );
    }

    @Test
    public void testSublist() throws Exception {
        assertThat( list.subList( list.indexOf( 1 ), list.lastIndexOf( 2 ) ), contains( "a", "b", "cc" ) );
    }
}
