package com.xenoterracide.datafactory;

import java.util.function.Function;
import java.util.function.Supplier;

public interface DataFactoryOld {

    default String getWord() {
        return getWordSupplier().get();
    }

    Supplier<String> getWordSupplier();

    default String getPage( final int maxInclusive ) {
        StringBuilder builder = new StringBuilder();
        int nextLength = builder.length();
        String next = "";
        String prev = "";
        while ( !( nextLength >= maxInclusive ) ) {
            if ( !prev.isEmpty() ) {
                builder.append( "\n\n" );
            }
            if ( !next.isEmpty() && !( nextLength >= maxInclusive ) ) {
                builder.append( next );
            }
            prev = next;
            next = this.getParagraph( 200 );
            nextLength = next.length() + builder.length() + 1;
        }
        return builder.toString();
    }

    default String getParagraph( final int maxInclusive ) {
        StringBuilder builder = new StringBuilder();
        int nextLength = builder.length();
        String next = "";
        String prev = "";
        while ( !( nextLength >= maxInclusive ) ) {
            if ( !prev.isEmpty() ) {
                builder.append( "  " );
            }
            if ( !next.isEmpty() && !( nextLength >= maxInclusive ) ) {
                builder.append( next );
            }
            prev = next;
            /// next = this.getSentence();
            nextLength = next.length() + builder.length() + 1;
        }
        return builder.toString();
    }

    Function<Integer, Integer> getRandomSupplier();

    Supplier<Character> getPunctuationSupplier();
}
