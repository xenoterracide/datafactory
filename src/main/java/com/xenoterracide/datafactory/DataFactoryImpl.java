package com.xenoterracide.datafactory;

import org.apache.commons.lang3.RandomUtils;

import java.util.List;
import java.util.function.IntBinaryOperator;

import static com.xenoterracide.datafactory.DataFactoryUtil.dict;

public class DataFactoryImpl implements DataFactory {

    private final IntBinaryOperator random;
    private final List<String> wordSource;

    DataFactoryImpl( final IntBinaryOperator randomSource, final List<String> wordSource ) {
        this.random = randomSource;
        this.wordSource = IndexedList.createFrom( wordSource );
    }

    public static DataFactory defaultInstance() {
        return new DataFactoryImpl( RandomUtils::nextInt, dict() );
    }

    @Override
    public IntBinaryOperator getRandom() {
        return random;
    }

    @Override
    public List<String> getWordSource() {
        return this.wordSource;
    }

}
