package com.xenoterracide.datafactory;

import org.apache.commons.lang3.RandomStringUtils;

import java.util.List;
import java.util.function.IntBinaryOperator;

public interface DataFactory {

    default RandomStringFactory numeric() {
        return ( min, max ) -> RandomStringUtils.randomNumeric( getRandom().applyAsInt( min, max ) );
    }

    IntBinaryOperator getRandom();

    default RandomStringFactory alphanumeric() {
        return ( min, max ) -> RandomStringUtils.randomAlphanumeric( getRandom().applyAsInt( min, max ) );
    }

    default RandomStringFactory graph() {
        return ( min, max ) -> RandomStringUtils.random( getRandom().applyAsInt( min, max ), 33, 126, false, false );
    }

    default RandomStringFactory print() {
        return ( min, max ) -> RandomStringUtils.random( getRandom().applyAsInt( min, max ), 32, 126, false, false );
    }

    default RandomStringFactory sentence() {
        return ( min, max ) -> {
            StringBuilder builder = new StringBuilder();
            int nextLen = builder.length();
            String next = "";
            String prev = "";
            int target = getRandom().applyAsInt( min, max );
            while ( !( nextLen > target ) ) {
                int thisLen = nextLen;
                if ( !prev.isEmpty() && !( nextLen + 1 > max ) ) {
                    thisLen += 1;
                    builder.append( " " );
                }
                if ( !next.isEmpty() && !( thisLen > max ) ) {
                    builder.append( next );
                }
                /*
                Integer randomInt = getRandom().applyAsInt( min, maxSize( max, getWordSource().entrySet() ) );
                if ( randomInt % 3 == 0 ) {
                    builder.append( this.getPunctuationSupplier().get() );
                }
                */
                prev = next;
                next = word().getRange( min, max );
                nextLen = next.length() + builder.length();
            }
            return builder.toString();
        };
    }

    default RandomStringFactory word() {
        return ( min, max ) -> {
            List<String> inRange = DataFactoryUtil.valuesInRange( getWordSource(), min, max );
            int random = getRandom().applyAsInt( 0, inRange.size() );
            String s = inRange.get( random );
            return s == null ? alphabetic().getRange( min, max ) : s;
        };
    }

    List<String> getWordSource();

    default RandomStringFactory alphabetic() {
        return ( min, max ) -> RandomStringUtils.randomAlphabetic( getRandom().applyAsInt( min, max ) );
    }
}
