package com.xenoterracide.datafactory;

import com.google.common.collect.ComparisonChain;
import com.google.common.collect.Range;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

final class DataFactoryUtil {
    private DataFactoryUtil() {
    }

    static List<String> dict() {
        try {
            URL url = DataFactoryUtil.class.getClassLoader().getResource( "dictionary" );
            assert url != null;
            return Files.lines( Paths.get( url.toURI() ) ).collect( Collectors.toList() );
        }
        catch ( URISyntaxException | IOException e ) {
            throw new IllegalStateException( e );
        }
    }

    static Comparator<String> sortByLength() {
        return ( o1, o2 ) -> ComparisonChain.start()
                .compare( o1.length(), o2.length() )
                .compare( o1, o2 )
                .result();
    }

    static List<String> valuesInRange( final List<String> source, final int min, final int max ) {
        int start = source.indexOf( min );
        int end = source.lastIndexOf( max );

        if ( notFound( start ) || notFound( end ) ) { // optimizations likely possible
            return source.stream()
                    .filter( s -> Range.closed( min, max ).contains( s.length() ) )
                    .collect( Collectors.toList() );
        }

        return source.subList( start, end );
    }

    static boolean notFound( final int index ) {
        return Objects.equals( -1, index );
    }
}
