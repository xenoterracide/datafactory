package com.xenoterracide.datafactory;

import com.google.common.collect.ForwardingList;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

class IndexedList extends ForwardingList<String> {

    private final List<String> list;
    private final Map<Integer, Pair<Integer, Integer>> index;

    protected IndexedList( final List<String> list, final Map<Integer, Pair<Integer, Integer>> index ) {
        this.list = list;
        this.index = index;
    }

    static IndexedList createFrom( final List<String> list ) {
        Collections.sort( list, DataFactoryUtil.sortByLength() );

        Map<Integer, Pair<Integer, Integer>> index = new HashMap<>();
        int prev = 0;
        int last = 0;
        int curr = 0;
        for ( String e : list ) {
            int next = e.length();
            curr++;

            if ( !Objects.equals( prev, next ) ) {
                prev = next;
                index.put( next, Pair.of( last, curr ) );
            }
        }
        return new IndexedList( list, index );
    }

    @Override
    protected List<String> delegate() {
        return list;
    }

    @Override
    public int indexOf( final Object element ) {
        if ( element instanceof Integer ) {
            Pair<Integer, Integer> pair = index.get( element );
            return pair == null ? -1 : pair.getLeft();
        }
        return super.indexOf( element );
    }

    @Override
    public int lastIndexOf( final Object element ) {
        if ( element instanceof Integer ) {
            Pair<Integer, Integer> pair = index.get( element );
            return pair == null ? -1 : pair.getRight();
        }
        return super.lastIndexOf( element );
    }
}
